#include <assert.h>
#include <ctype.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>

sd_bus *bus;
static int debug = 0;

int croak(char *message, int error_code) {
  printf("FATAL ERROR %s: %s\n", message, strerror(error_code));
  exit(1);
}

bool bus_type_is_container(char c) {
  static const char valid[] = {SD_BUS_TYPE_ARRAY, SD_BUS_TYPE_VARIANT,
                               SD_BUS_TYPE_STRUCT, SD_BUS_TYPE_DICT_ENTRY};

  return !!memchr(valid, c, sizeof(valid));
}

int format_type(sd_bus_message *m) {
  int r;

  for (;;) {
    const char *contents = NULL;
    char type;
    union {
      uint8_t u8;
      uint16_t u16;
      int16_t s16;
      uint32_t u32;
      int32_t s32;
      uint64_t u64;
      int64_t s64;
      double d64;
      const char *string;
      int i;
    } basic;

    r = sd_bus_message_peek_type(m, &type, &contents);
    if (r < 0)
      croak("Failed to peek type", -r);

    // end of container
    if (r == 0)
      return 0;

    if (bus_type_is_container(type) > 0) {
      r = sd_bus_message_enter_container(m, type, contents);
      if (r < 0)
        croak("Failed to enter array message container", -r);

      if (type == SD_BUS_TYPE_ARRAY) {
        unsigned n = 0;

        /* count array entries */
        for (;;) {
          r = sd_bus_message_skip(m, contents);
          if (r < 0)
            croak("Failed to skip over array entry", -r);

          if (r == 0)
            break;

          n++;
        }

        r = sd_bus_message_rewind(m, false);
        if (r < 0)
          croak("Failed to rewind message", -r);

        printf("array size: %u\n", n);

      } else if (type == SD_BUS_TYPE_VARIANT) {
        printf("variant: %s\n", contents);
      }

      r = format_type(m);

      r = sd_bus_message_exit_container(m);
      if (r < 0)
        croak("Failed to exit container", -r);

      continue;
    }

    r = sd_bus_message_read_basic(m, type, &basic);
    if (r < 0)
      return r;

    switch (type) {
    case SD_BUS_TYPE_BYTE:
      printf("byte: %u\n", basic.u8);
      break;

    case SD_BUS_TYPE_BOOLEAN:
      printf("boolean: %c\n", basic.i);
      break;

    case SD_BUS_TYPE_INT16:
      printf("int16: %i\n", basic.s16);
      break;

    case SD_BUS_TYPE_UINT16:
      printf("uint16: %u\n", basic.u16);
      break;

    case SD_BUS_TYPE_INT32:
      printf("int32: %i\n", basic.s32);
      break;

    case SD_BUS_TYPE_UINT32:
      printf("uint32: %u\n", basic.u32);
      break;

    case SD_BUS_TYPE_INT64:
      printf("int64: %" PRIi64 "\n", basic.s64);
      break;

    case SD_BUS_TYPE_UINT64:
      printf("uint64: %" PRIu64 "\n", basic.u64);
      break;

    case SD_BUS_TYPE_DOUBLE:
      printf("double: %g\n", basic.d64);
      break;

    case SD_BUS_TYPE_STRING:
    case SD_BUS_TYPE_OBJECT_PATH:
    case SD_BUS_TYPE_SIGNATURE:
      printf("string|path|signature: '%s'\n", basic.string);
      break;

    case SD_BUS_TYPE_UNIX_FD:
      printf("unix_fd: %i", basic.i);
      break;

    default:
      croak("Unknown basic type", 0);
    }
  }
}

void get_properties(const char *service, const char *path,
                    const char *interface) {
  sd_bus_error error = SD_BUS_ERROR_NULL;
  int r;
  sd_bus_message *reply = NULL;

  printf(
      "Attempting to fetch app properties: service=%s path=%s interface=%s\n",
      service, path, interface);

  r = sd_bus_call_method(bus, service, path, "org.freedesktop.DBus.Properties",
                         "GetAll", &error, &reply, "s", interface);

  if (r < 0)
    croak("Failed to get properties of interface", -r);

  if (debug) {
    r = sd_bus_message_dump(reply, stdout, SD_BUS_MESSAGE_DUMP_WITH_HEADER);
    if (r < 0)
      croak("Failed to dump message", -r);

    r = sd_bus_message_rewind(reply, 0);
    if (r < 0)
      croak("Failed to rewind message", -r);
  }

  r = sd_bus_message_enter_container(reply, 'a', "{sv}");
  if (r < 0)
    croak("Failed to enter array message container", -r);

  for (;;) {
    // Member *z;
    // char *buf = NULL;
    // FILE *mf = NULL;
    // size_t sz = 0;
    const char *name;

    r = sd_bus_message_enter_container(reply, 'e', "sv");
    if (r < 0)
      croak("Failed to enter dict entry message container", -r);

    if (r == 0)
      break;

    r = sd_bus_message_read(reply, "s", &name);
    if (r < 0)
      croak("Failed to read message", -r);

    printf("Property: name=%s\n", name);

    // r = sd_bus_message_enter_container(reply, 'v', NULL);
    // if (r < 0)
    //   croak("Failed to enter variant message container: %s", -r);

    // mf = open_memstream_unlocked(&buf, &sz);
    // if (!mf)
    //   return log_oom();
    //
    format_type(reply);
    // if (r < 0)
    //   return bus_log_parse_error(r);
    //
    // mf = safe_fclose(mf);
    //
    // z = set_get(members, &((Member) {
    //   .type = "property",
    //   .interface = m->interface,
    //   .name = (char*) name }));
    //
    // if (z)
    //   free_and_replace(z->value, buf);

    // r = sd_bus_message_exit_container(reply);
    // if (r < 0)
    //   croak("Failed to exit variant message container", -r);

    r = sd_bus_message_exit_container(reply);
    if (r < 0)
      croak("Failed to exit dict entry message container", -r);
  }

  r = sd_bus_message_exit_container(reply);
  if (r < 0)
    croak("Failed to exit array message container", -r);

  return;
}

void get_property(const char *service, const char *path, const char *interface,
                  const char *property) {
  sd_bus_error error = SD_BUS_ERROR_NULL;
  int r;
  sd_bus_message *reply = NULL;
  const char *contents = NULL;
  char type;

  printf("Attempting to fetch: service=%s path=%s interface=%s property=%s\n",
         service, path, interface, property);

  r = sd_bus_call_method(bus, service, path, "org.freedesktop.DBus.Properties",
                         "Get", &error, &reply, "ss", interface, property);

  if (r < 0)
    croak("Failed to get property", -r);

  r = sd_bus_message_peek_type(reply, &type, &contents);
  if (r < 0)
    croak("Failed to peek message type", -r);

  r = sd_bus_message_enter_container(reply, 'v', contents);
  if (r < 0)
    croak("Failed to enter message container", -r);

  if (debug) {
    r = sd_bus_message_dump(reply, stdout, SD_BUS_MESSAGE_DUMP_WITH_HEADER);
    if (r < 0)
      croak("Failed to dump message", -r);

    r = sd_bus_message_rewind(reply, 0);
    if (r < 0)
      croak("Failed to rewind message", -r);
  }

  format_type(reply);
  // printf("contents=%s type=%c\n", contents, type);
  // if (strncmp(contents, "a", 1) == 0) {
  //   printf("Got an array\n");
  //   // parse_array(reply);
  // } else {
  //   // return message_to_sv(reply, type);
  // }
  // r = sd_bus_message_exit_container(reply);
  // if (r < 0)
  //     croak("Failed to exit message container: %s", strerror(-r));

  return;
}

int main(int argc, char **argv) {
  int c, r;
  static int use_system_bus = 0;
  static const char *service = NULL;
  static const char *object = NULL;
  static const char *interface = NULL;
  static const char *property = NULL;

  static struct option long_options[] = {
      {"system", no_argument, &use_system_bus, 1},
      {"debug", no_argument, &debug, 'd'},
      {"service", required_argument, 0, 's'},
      {"object", required_argument, 0, 'o'},
      {"interface", required_argument, 0, 'i'},
      {"property", required_argument, 0, 'p'},
      {NULL, 0, NULL, 0},
  };

  int option_index = 0;
  while ((c = getopt_long(argc, argv, "s:o:i:p:d", long_options,
                          &option_index)) != -1) {
    switch (c) {
    case 0:
      printf("option %s", long_options[option_index].name);
      if (optarg)
        printf(" with arg %s", optarg);

      printf("\n");

      break;
    case 's':
      service = optarg;
      break;
    case 'o':
      object = optarg;
      break;
    case 'i':
      interface = optarg;
      break;
    case 'p':
      property = optarg;
      break;
    case 'd':
      break;
    }
  }

  if (use_system_bus) {
    printf("Connecting to system bus\n");
    r = sd_bus_default_system(&bus);
  } else {
    printf("Connecting to session bus\n");
    r = sd_bus_default_user(&bus);
  }

  assert(r >= 0);

  if (property != NULL)
    get_property(service, object, interface, property);
  else
    get_properties(service, object, interface);

  exit(0);
}
